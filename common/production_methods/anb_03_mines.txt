﻿## Coal Mine
## Iron Mine
pm_deposit_divination_coal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_reagents_add = 10
			
			# output goods
			goods_output_coal_add = 45
		}

		level_scaled = {
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_coal_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_coal_add = 115
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_coal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 7
			
			# output goods
			goods_output_coal_add = 15
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_target_selecting_explosions_coal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_coal_add = 45
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_automata_miners = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
		
	unlocking_technologies = {
		steam_donkey
	}

	disallowing_laws = {
		law_industry_banned
	}
		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_automata_add = 5
			}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_foremen = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
		
	unlocking_technologies = {
		steam_donkey
	}

	disallowing_laws = {
		law_industry_banned
	}
		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_automata_add = 11
			}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1750
		}
	}
}

## Iron Mine
pm_deposit_divination_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_reagents_add = 10
			
			# output goods
			goods_output_iron_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_iron_add = 90
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 7
			
			# output goods
			goods_output_iron_add = 12
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_target_selecting_explosions_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_iron_add = 35
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

## Lead Mine
pm_deposit_divination_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_reagents_add = 10
			
			# output goods
			goods_output_iron_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_lead_add = 90
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 7
			
			# output goods
			goods_output_lead_add = 12
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_target_selecting_explosions_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_lead_add = 35
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

## Sulfur Mine
pm_deposit_divination_sulfur_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_reagents_add = 10
			
			# output goods
			goods_output_sulfur_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_sulfur_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_sulfur_add = 95
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 7
			
			# output goods
			goods_output_sulfur_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_target_selecting_explosions_sulfur_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_sulfur_add = 35
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

## Gold Mine
pm_deposit_divination_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_reagents_add = 10
			
			# output goods
			goods_output_gold_add = 20
		}

		level_scaled = {
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 1000
		}
	}
}

pm_transmutative_heavy_pump_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_gold_add = 50
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 2500
		}
	}
}

pm_evocation_spells_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_reagents_add = 7
			
			# output goods
			goods_output_gold_add = 5
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_target_selecting_explosions_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_gold_add = 18
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 900
		}
	}

}