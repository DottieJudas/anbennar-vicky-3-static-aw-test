﻿# law_banned_adventurers = {
	# group = lawgroup_right_to_adventure
	
	# icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	# on_enact = {
		# recalculate_pop_ig_support = yes
	# }
	
	# modifier = {
		# country_authority_add = 200
	# }
	
	# possible_political_movements = {
		# law_sponsored_adventurers
		# law_military_adventurers
		# law_state_adventurers
	# }
# }

# law_freedom_to_adventure = {
	# group = lawgroup_right_to_adventure
	
	# icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	# on_enact = {
		# recalculate_pop_ig_support = yes
	# }
	
	# modifier = {
		# interest_group_ig_landowners_pol_str_mult = 0.10 # These are the guys hiring adventurers
	# }
	
	# possible_political_movements = {
		# law_banned_adventurers
	# }
# }

# law_military_adventurers = {
	# group = lawgroup_right_to_adventure
	
	# icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	# on_enact = {
		# recalculate_pop_ig_support = yes
	# }
	
	# unlocking_technologies = {
		# law_enforcement
	# }
	
	# modifier = {
		# interest_group_ig_armed_forces_pol_str_mult = 0.15 # Essentially a license to allow the military to kill dudes across the country with minimal oversight
	# }
	
	# possible_political_movements = {
		# law_state_adventurers
		# law_sponsored_adventurers
		# law_banned_adventurers
	# }
# }

# law_chronicler_adventurers = {
	# group = lawgroup_right_to_adventure
	
	# icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	# on_enact = {
		# recalculate_pop_ig_support = yes
	# }
	
	# modifier = {
		# interest_group_ig_intelligensia_pol_str_mult = 0.10 # These are the guys hiring adventurers
	# }
	
	# possible_political_movements = {
		# law_military_adventurers
		# law_sponsored_adventurers
		# law_banned_adventurers
	# }
# }

# law_adventurer_licenses = {
	# group = lawgroup_right_to_adventure
	
	# icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	# on_enact = {
		# recalculate_pop_ig_support = yes
	# }
	
	# modifier = {
	# }
	
	# possible_political_movements = {
		# law_military_adventurers
		# law_sponsored_adventurers
		# law_banned_adventurers
	# }
# }