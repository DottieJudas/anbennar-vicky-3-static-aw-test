﻿
#Serpentbloom Farms
pm_serpentbloom_opium = {
	texture = "gfx/interface/icons/production_method_icons/potatoes.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_grain_add = -20
			goods_output_opium_add = 10
		}
	}
}

#Mushroom Farms
pm_mushroom_liquor = {
	texture = "gfx/interface/icons/production_method_icons/potatoes.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_grain_add = -20
			goods_output_liquor_add = 15
		}
	}
}


#Cave Coral
pm_simple_forestry_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/simple_forestry.dds"

	building_modifiers = {
		workforce_scaled = {
			goods_output_wood_add = 20  #Default 30
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = 4500
		}
	}
}
pm_saw_mills_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/saw_mills.dds"
	unlocking_technologies = {
		steelworking
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_wood_add = 40 #Default 60
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
}	
pm_electric_saw_mills_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/electric_saw_mills.dds"
	unlocking_technologies = {
		electrical_generation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_electricity_add = 5
			
			# output goods
			goods_output_wood_add = 80 #Default 100
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 500
		}
	}

	required_input_goods = electricity
}

pm_no_hardwood = {
	texture = "gfx/interface/icons/production_method_icons/no_hardwood_selection.dds"
}
pm_hardwood_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/hardwood_selection.dds"
	building_modifiers = {
		workforce_scaled = {
			# output goods										
			goods_output_wood_add = -20
			goods_output_hardwood_add = 15
		}
	}
}
pm_increased_hardwood_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/increased_hardwood.dds"

	unlocking_production_methods = {
		pm_saw_mills_cave_coral
		pm_electric_saw_mills_cave_coral
	}

	building_modifiers = {
		workforce_scaled = {
			# output goods
			goods_output_wood_add = -40
			goods_output_hardwood_add = 30
		}
	}
}

pm_merchant_guilds_building_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_simple_forestry_cave_coral
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}
pm_privately_owned_building_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_production_methods = {
		pm_saw_mills_cave_coral
		pm_electric_saw_mills_cave_coral
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}
pm_publicly_traded_building_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/publicly_traded.dds"

	unlocking_production_methods = {
		pm_saw_mills_cave_coral
		pm_electric_saw_mills_cave_coral
	}
	
	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	unlocking_technologies = {
		mutual_funds
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 150
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}
pm_government_run_building_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/government_run.dds"

	unlocking_laws = {
		law_command_economy
	}
	
	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 150
		}
		unscaled = {
			building_government_shares_add = 1
		}
	}
}
pm_worker_cooperative_building_cave_coral = {
	texture = "gfx/interface/icons/production_method_icons/worker_cooperative.dds"

	unlocking_production_methods = {
		pm_saw_mills_cave_coral
		pm_electric_saw_mills_cave_coral
	}

	unlocking_laws = {
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 250
		}
		unscaled = {
			building_workforce_shares_add = 1
		}
	}
}


#Mithril Mines
pm_picks_and_shovels_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_mithril_add = 15
		}

		level_scaled = {
			building_employment_laborers_add = 4500
		}
	}
}
pm_atmospheric_engine_pump_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"

	unlocking_technologies = {
		atmospheric_engine
	}
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_mithril_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
}
pm_condensing_engine_pump_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"

	unlocking_technologies = {
		watertube_boiler
	}
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_mithril_add = 50
		}

		level_scaled = {
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 250
		}
	}
}
pm_diesel_pump_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			
			# output goods
			goods_output_mithril_add = 60
		}

		level_scaled = {
			building_employment_laborers_add = 2000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 500
		}
	}
}

pm_nitroglycerin_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/nitroglycerin.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		nitroglycerin
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 5
			
			# output goods
			goods_output_mithril_add = 8
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.3
			building_machinists_mortality_mult = 0.2
			building_engineers_mortality_mult = 0.1
		}
	}
}
pm_dynamite_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/dynamite.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		dynamite
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 10
			
			# output goods
			goods_output_mithril_add = 15
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_merchant_guilds_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_picks_and_shovels_building_mithril_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}
pm_privately_owned_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_mithril_mine
		pm_condensing_engine_pump_building_mithril_mine
		pm_diesel_pump_building_mithril_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}
pm_publicly_traded_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/publicly_traded.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_mithril_mine
		pm_condensing_engine_pump_building_mithril_mine
		pm_diesel_pump_building_mithril_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	unlocking_technologies = {
		mutual_funds
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 150
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}
pm_government_run_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/government_run.dds"
	
	unlocking_laws = {
		law_command_economy
	}

	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 150
		}
		unscaled = {
			building_government_shares_add = 1
		}
	}
}
pm_worker_cooperative_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/worker_cooperative.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_mithril_mine
		pm_condensing_engine_pump_building_mithril_mine
		pm_diesel_pump_building_mithril_mine
	}

	unlocking_laws = {
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 250
		}
		unscaled = {
			building_workforce_shares_add = 1
		}
	}
}


#Gem Mines

pm_picks_and_shovels_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_reagents_add = 10
		}

		level_scaled = {
			building_employment_laborers_add = 4500
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 150 #Gold = 500,  30%
		}
	}
}
pm_atmospheric_engine_pump_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		atmospheric_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_magical_reagents_add = 20
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 300 #Gold = 1000, 30%
		}
	}	
}
pm_condensing_engine_pump_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		watertube_boiler
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_reagents_add = 40
		}

		level_scaled = {
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 250
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 450 #Gold = 1250, 36%
		}
	}	
}
pm_diesel_pump_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		compression_ignition
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			
			# output goods
			goods_output_magical_reagents_add = 60
		}

		level_scaled = {
			building_employment_laborers_add = 2000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 500
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 675 #Gold = 1500, 45%
		}
	}
}

pm_merchant_guilds_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_picks_and_shovels_building_gem_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}
pm_privately_owned_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_gem_mine
		pm_condensing_engine_pump_building_gem_mine
		pm_diesel_pump_building_gem_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}
pm_publicly_traded_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/publicly_traded.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_gem_mine
		pm_condensing_engine_pump_building_gem_mine
		pm_diesel_pump_building_gem_mine
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	unlocking_technologies = {
		mutual_funds
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 150
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}
pm_government_run_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/government_run.dds"
	
	unlocking_laws = {
		law_command_economy
	}

	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 150
		}
		unscaled = {
			building_government_shares_add = 1
		}
	}
}
pm_worker_cooperative_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/worker_cooperative.dds"

	unlocking_production_methods = {
		pm_atmospheric_engine_pump_building_gem_mine
		pm_condensing_engine_pump_building_gem_mine
		pm_diesel_pump_building_gem_mine
	}

	unlocking_laws = {
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 250
		}
		unscaled = {
			building_workforce_shares_add = 1
		}
	}
}