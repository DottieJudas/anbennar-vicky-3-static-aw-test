﻿POPS = {
	s:STATE_LAKE_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 1181100
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 88900
			}
		}
	}
	s:STATE_MOUNTAIN_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 916400
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 150800
			}
			create_pop = {
				culture = emerald_orc
				size = 92800
			}
		}
	}
	s:STATE_THORN_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 516800
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 61200
			}
			create_pop = {
				culture = emerald_orc
				size = 102000
			}
		}
	}
	s:STATE_HUNTER_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 655700
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 74700
			}
			create_pop = {
				culture = emerald_orc
				size = 99600
			}
		}
	}
	s:STATE_FLOWER_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 691260
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 37720
			}
			create_pop = {
				culture = emerald_orc
				size = 118020
			}
		}
	}
	s:STATE_RIVER_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 860320
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 90560
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 181120
			}
		}
	}
	s:STATE_SHADOW_GROVE = {
		region_state:A79 = {
			create_pop = {
				culture = wood_elven
				size = 280500
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 246840
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 194480
			}
			create_pop = {
				culture = firanyan_harpy
				size = 26180
			}
		}
	}
	s:STATE_ARROW_GROVE = {
		region_state:A80 = {
			create_pop = {
				culture = wood_elven
				size = 7740
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 208980
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 557280
			}
		}
	}
	s:STATE_BONE_GROVE = {
		region_state:A80 = {
			create_pop = {
				culture = wood_elven
				size = 19640
			}
			create_pop = {
				culture = karakhanbari_orc
				size = 196400
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 765960
			}
		}
	}
}