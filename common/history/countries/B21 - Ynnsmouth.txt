﻿COUNTRIES = {
	c:B21 = {
		effect_starting_technology_tier_3_tech = yes

		effect_starting_politics_reactionary = yes
		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_parliamentary_republic #Was a kingdom until very recently hence the more restictive laws
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		# No Police force
		activate_law = law_type:law_no_schools
		# No health
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		# No migration controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_non_monstrous_only
	}
}